# Model
* Multiple elevators
* The user is going to specify the destination floor, but the user will only know what elevator he has to hop in when it arrives.
* It's going to go all the way up before going down
* I want the fitness function to factor in several times like the time to get inside the elevator, the time to get out, etc...
* How the requests come in through time. I think it would be interesting to see how the the average service time for the algorithm behaves in a simulation environment. We can model the arrival of different requests using a Poisson distribution
* The elevators should have a maximum capacity and not accept requests when they're capacity is maxed out.

# Assumptions
* Assume that each request corresponds to a single person
* If we have multiple people wanting to get in from the same floor, there will be multiple requests. This is a reasonable assumption considering the number of people that actually correspond to a single request could be easily modeled using a different sensor, like a camera for each floor


# Chromosome structure
Each individual in our population is represented by a single string of numbers, where each number represents the id of the elevator that the request is assigned to. The number of current requests changes dynamically, since a new request can come or a previous request can be finished. In any case, the size of the chromosome changes, but we only rerun our genetic algorithm once a new request comes in and we must satisfy it.
Suppose we have 4 elevators and the following request sequence in time:

| t | 0 | 1 | 2 | 3 | 4 |
| - | - | - | - | - | - |
| event | H(3, 4) | H(3, 8) | H(2, 7) | H(1, 9) |
| chromosome | 1 | 1;1 | 1;1;2;


* What happens if all elevators are full?

# Fitness
If the elevator is going up, we can compute the journey time for queued requests and ongoing requests as follows:
* Direction flags:
  * 1UP, DOWN, 2UP
* Max and min floor computation:
  * MAX = max(ongoing_dst_floors, src_floors, dst_floors of src_floors > cur_floor)
  * MIN = min(ongoing_dst_floors, src_floors, dst_floors)
* Queued requests:
  * if src_floor > car_floor:
    * if src_floor < dst_floor:
      * (dst_floor - car_floor) * itt + nr_stops * pt
      * dir_flag = 1UP
    * else:
      * (MAX - cur_floor + MAX - dst_floor) * itt + nr_stops * pt
      * dir_flag = DOWN
  * else:
    * if src_floor < dst_floor:
      * (MAX - cur_floor + MAX - MIN + dst_floor - MIN) * itt + nr_stops * pt (tricky to compute nr_stops)
      * dir_flag = 2UP
    * else:
      * (MAX - cur_floor + MAX - dst_floor) * itt + nr_stops * pt
      * dir_flag = DOWN
* Current requests:
  * if ongoing_dst_floor > cur_floor:
    * (ongoing_dst_floor - cur_floor) * itt + nr_stops*pt
    * dir_flag = 1UP
  * else:
    * (MAX - cur_floor + MAX - dst_floor)  * itt + nr_stops * pt
    * dir_flag = DOWN

# TODO
* Fix any issues with the vanilla genetic algorithm
* Implement remaining genetic algorithm requirements in specs
* Create a request class that saves the start time of each request
* Plot the maximum, mean, and minimum fitness for each generation given a set of requests
* Compute the mean generation of convergence for each genetic algorithm configuration
* Maybe all tests should be run with a random set of requests and an empty elevator, just to see what algorithm is best
* The simulation is just extra stuff

# Variations
* Population size:
  * 20
  * 40
* Crossover rate:
  * 70%
  * 90%
* Mutation rate:
  * 5%
  * 1%
* Stopping criteria:
  * Number of generations
  * At least 50% of the population is the same
* Crossover technique:
  * One crossover point
  * Two crossover points
* Mutation technique:
  * Change the value of one gene
  * Swap two neighboring genes
* Substitution method:
  * Replace entire population 1
  * Add the fittest individual from population 1
* Selection technique:
  * Russian roulette
  * Create several mini tournaments where the fittest individual is always selected
