import numpy as np

def sparse_mean(arrays):

    # Find array with max length
    max_len = 0
    for arr in arrays:
        if len(arr) > max_len:
            max_len = len(arr)

    # Create a numpy masked array to compute the means
    masked_array = np.ma.empty((len(arrays), max_len))
    masked_array.mask = True
    for row, arr in enumerate(arrays):
        masked_array[row, :len(arr)] = arr

    return np.mean(masked_array, axis=0)
