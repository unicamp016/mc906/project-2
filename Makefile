PYTHON = python3.6

# Directories
OUT_DIR = bin
SRC_DIR = src
LOG_DIR = logs
REPORT_DIR = report
POSTER_DIR = poster

REPORT = report
POSTER = poster
MAIN = main

.PHONY: run clean build write-report

submit: build
	@mkdir $(OUT_DIR)/code
	@rsync -azP --exclude=".git*" --exclude-from=.gitignore . $(OUT_DIR)/code
	@tar -cz -C $(OUT_DIR) $(REPORT).pdf $(POSTER).pdf code -f $(OUT_DIR)/projeto-2.tar.gz
	@rm -rf $(OUT_DIR)/code

build: $(OUT_DIR)/$(REPORT).pdf $(OUT_DIR)/$(POSTER).pdf

clean:
	@find . -name "*.pyc" -delete
	@rm -rf $(OUT_DIR)/* $(LOG_DIR)/*

save:
	@mkdir -p $(OUT_DIR) $(LOG_DIR)
	$(PYTHON) $(SRC_DIR)/$(MAIN).py -o $(OUT_DIR) -l $(LOG_DIR) --show
	
run:
	@mkdir -p $(OUT_DIR) $(LOG_DIR)
	$(PYTHON) $(SRC_DIR)/$(MAIN).py -o $(OUT_DIR) -l $(LOG_DIR)

write-report: $(REPORT_DIR)/$(REPORT).tex
	latexmk -pdf -pvc -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)

$(OUT_DIR)/$(POSTER).pdf: $(POSTER_DIR)/$(POSTER).tex
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)

$(OUT_DIR)/$(REPORT).pdf: $(REPORT_DIR)/$(REPORT).tex
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)
