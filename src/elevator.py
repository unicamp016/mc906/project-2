import collections
import simpy

memid = lambda x: id(x)

class Elevator:
    def __init__(self, env, id, nr_floors, pending_reqs, passive_time, inter_floor_time):
        self.env = env
        self.id = id
        self.nr_floors = nr_floors
        self.passive_time = passive_time
        self.inter_floor_time = inter_floor_time
        self.pending_reqs = pending_reqs
        self.ongoing_reqs = []
        self.floor_stops = []
        self.last_departure_time = None
        self.direction = 1
        self.last_floor = 0
        self.process = self.env.process(self.update())
        self._cur_floor = None
        self.traveling = True

    def update(self):
        start_time = -1
        while True:
            if self.floor_stops:
                if self.traveling:
                    try:
                        first_stop = self.floor_stops[0]
                        print(f"cur_floor={self.cur_floor}, first_stop={first_stop}")
                        if self.cur_floor <= first_stop:
                            if self.direction == 0:
                                print("Setting direction to 1 while moving")
                                self.last_floor = self.cur_floor
                                self.last_departure_time = self.env.now
                                self.direction = 1      # Going up
                        else:
                            if self.direction == 1:
                                print("Setting direction to 0 while moving")
                                self.last_floor = self.cur_floor
                                self.last_departure_time = self.env.now
                                self.direction = 0      # Going down

                        delay = abs(self.cur_floor - first_stop) * self.inter_floor_time
                        print(f"Programmed event for elev {self.id} at {self.env.now} + {delay} = {self.env.now + delay}\n")
                        if delay > 0:
                            yield self.env.timeout(delay=delay)
                        print(f"Stopped at floor {first_stop} at time {self.env.now}, updating structs\n")
                        self.last_floor = first_stop
                        if len(self.floor_stops) > 1:
                            if self.last_floor <= self.floor_stops[1]:
                                print("Setting direction to 1 after stopping")
                                self.direction = 1
                            else:
                                print("Setting direction to 0 after stopping")
                                self.direction = 0
                        self.floor_stops.remove(first_stop)
                        self.update_ongoing_requests(src_floor=first_stop)
                        self.traveling = False
                    except simpy.Interrupt:
                        print("Trip was interrupted by new request, update event...")
                else:
                    if start_time < 0:
                        start_time = self.env.now
                    try:
                        delay = self.passive_time - (self.env.now - start_time)
                        yield self.env.timeout(delay=delay)
                        start_time = -1
                        self.last_departure_time = self.env.now
                        self.traveling = True
                    except simpy.Interrupt:
                        print("Passive time was interrupted")
            else:
                try:
                    print("Going to sleep zzzzzzzz.....")
                    self.last_floor = self.cur_floor
                    self.idle = True
                    yield self.env.timeout(delay=9999999)
                except simpy.Interrupt:
                    self.idle = False
                    self.last_departure_time = self.env.now
                    print(f"Starting to process stops for elev {self.id}")

    def update_ongoing_requests(self, src_floor):
        if src_floor in self.ongoing_reqs:
            self.ongoing_reqs.remove(src_floor)
        for req in self.pending_reqs.copy():
            if self.id == req[0] and src_floor == req[1][0]:
                if req[1][1] > req[1][0] and self.direction == 1 or \
                   req[1][1] < req[1][0] and self.direction == 0:
                   self.ongoing_reqs.append(req[1][1])
                   self.pending_reqs.remove(req)

    @property
    def cur_floor(self):
        if self.last_departure_time is None:
            self._cur_floor = 0
        elif self.traveling and not self.idle:
            traveled_floors = (self.env.now - self.last_departure_time)/self.inter_floor_time
            if self.direction == 1:
                self._cur_floor = self.last_floor + traveled_floors
            else:
                self._cur_floor = self.last_floor - traveled_floors
        else:
            self._cur_floor = self.last_floor

        return self._cur_floor
