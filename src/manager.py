import os
import time

import simpy
import numpy as np
import matplotlib.pyplot as plt

import util
from elevator import Elevator

memid = lambda x: id(x)

class ElevatorManager:
    def __init__(self, env, nr_floors, nr_elevators, arrival_rate, passive_time,
     inter_floor_time, generations, population_size, crossover_rate,
     mutation_rate, output_dir, log_dir):
        self.env = env
        self.nr_floors = nr_floors
        self.nr_elevators = nr_elevators
        self.arrival_rate = arrival_rate
        self.passive_time = passive_time
        self.generations = generations
        self.population_size = population_size
        self.crossover_rate = crossover_rate
        self.mutation_rate = mutation_rate
        self.inter_floor_time = inter_floor_time
        self.output_dir = output_dir
        self.log_dir = log_dir
        self.pending_reqs = [] # (elev, (src, dst))

        # Has to be even in order for us to always select pairs of individuals
        assert self.population_size%2 == 0

        self.elevators = [
            Elevator(
                env=env,
                id=id,
                nr_floors=self.nr_floors,
                pending_reqs=self.pending_reqs,
                passive_time=self.passive_time,
                inter_floor_time=self.inter_floor_time
            ) for id in range(nr_elevators)]
        self.dispatcher_process = env.process(self.request_dispatcher(self.nr_floors, self.arrival_rate))

    def request_dispatcher(self, nr_floors, arrival_rate):
        seed = 1
        while self.env.now < 30:
            np.random.seed(seed)
            seed += 1
            origin = np.random.randint(0, nr_floors)
            dest = np.random.randint(0, nr_floors)
            while origin == dest:
                dest = np.random.randint(0, nr_floors)
            delay = np.random.poisson(arrival_rate)
            req = yield self.env.timeout(delay, value=(origin, dest))

            # Temporarily assign request to elevator 0, this will change once
            # we run the genetic algorithm
            self.pending_reqs.append((0, req))

            print(f'{self.env.now}:{req}')

            self.assign_elevators()

            print(f'After assigning elevators, pending reqs are {self.pending_reqs} ({memid(self.pending_reqs)})')

            elev_floor_stops = [[] for _ in range(self.nr_elevators)]
            elev_pending_reqs = [[] for _ in range(self.nr_elevators)]
            for i, req in enumerate(self.pending_reqs):
                elev_pending_reqs[req[0]].append(req[1])
            for id in range(self.nr_elevators):
                elev = self.elevators[id]
                self.calculate_stop_seq(id, elev_pending_reqs, elev_floor_stops, verbose=False)
                elev.floor_stops = elev_floor_stops[id]

                print(f"Info on {elev.id}: floor_stops={elev.floor_stops}, ongoing_reqs={elev.ongoing_reqs}, cur_floor={elev.cur_floor}, direction={elev.direction}, traveling={elev.traveling}")

                # if elev.floor_stops:
                elev.process.interrupt()

    def assign_elevators(self):
        population = self.generate_init_population()
        for gen in range(self.generations):
            fitness = self.compute_fitness(population)
            population = self.select(population, fitness)
            population = self.crossover(population)
            population = self.mutation(population)
        fitness = self.compute_fitness(population)
        fittest_individual = population[fitness.argmax()]
        new_pending_reqs = []
        for i in range(len(self.pending_reqs)):
            new_pending_reqs.append((fittest_individual[i], self.pending_reqs[i][1]))
        self.pending_reqs.clear()
        self.pending_reqs.extend(new_pending_reqs)

    def run_genetic_algorithm(self, config, all_requests):
        self.population_size = config['population-size']
        self.crossover_rate = config['crossover-rate']
        self.mutation_rate = config['mutation-rate']

        all_global_max_fits = []
        all_global_gens = []
        all_max_fits = []
        for requests in all_requests:
            self.pending_reqs = requests
            population = self.generate_init_population()
            keep_going = StoppingCriteria(config['stopping-criteria'], self)

            i = 0
            max_fit = []
            cur_max_fit = 0
            global_max_fit = 0
            global_gen = 0
            while keep_going(population, cur_max_fit):

                # Compute all individual fitnesses and find fittest individual
                fitness = self.compute_fitness(population)
                fittest_indiv = population[fitness.argmax()]

                # Keep track of fitness stats for plotting
                cur_max_fit = np.max(fitness)
                if cur_max_fit > global_max_fit:
                    global_max_fit = cur_max_fit
                    global_gen = i
                max_fit.append(cur_max_fit)
                i += 1

                # Apply genetic algorithm operators
                population = self.select(population, fitness, config['selection'])
                population = self.crossover(population, config['crossover'])
                population = self.mutation(population, config['mutation'])
                population = self.substitution(population, fittest_indiv, config['substitution'])

            # Compute fitness for final population
            fitness = self.compute_fitness(population)
            fittest_individual = population[fitness.argmax()]

            # Keep track of fitness stats for plotting
            cur_max_fit = np.max(fitness)
            if cur_max_fit > global_max_fit:
                global_max_fit = cur_max_fit
                global_gen = i
            max_fit.append(cur_max_fit)

            # Update lists with data from all iterations
            all_max_fits.append(max_fit)
            all_global_max_fits.append(global_max_fit)
            all_global_gens.append(global_gen)

        global_gen = np.mean(all_global_gens)
        global_max_fit = np.mean(all_global_max_fits)
        avg_max_fit = util.sparse_mean(all_max_fits)

        return (global_max_fit, global_gen), avg_max_fit

    def plot(self, param, max_fit_dict, config):

        fig, ax = plt.subplots(figsize=(10, 10))
        ax.set_ylabel('Fitness')
        ax.set_xlabel('Generations')

        for value, max_fits in max_fit_dict.items():
            xs = np.arange(len(max_fits))
            ax.plot(xs, max_fits, label=value)
        tokens = param.split('-')
        legend_title = ' '.join([t.capitalize() for t in tokens])
        ax.legend(title=legend_title)

        plt_title = f"compare-{param}"
        # for k, v in config.items():
        #     if k != param:
        #         plt_title += f"-{k}-{v}"
        fig.savefig(os.path.join(self.output_dir, f"{plt_title}.pdf"))

    def compare_genetic_algorithms(self, configs, iterations, nr_requests):
        # Generate set of requests that will be used throughout experiments
        all_requests = []
        for i in range(iterations):
            all_requests.append(self.generate_requests(nr_requests))

        logf = open(os.path.join(self.log_dir, 'comparisons.txt'), 'w')

        # Create a default config dict to use initially
        init_config = {}
        for param, values in configs.items():
            init_config[param] = values[0]

        best_config = None
        best_score = (0, float('inf'))
        best_avg_max_fit = None
        for param in configs:

            logf.write(f"Running tests for {param}\n")

            # Use our current best config to vary parameters
            if best_config is None:
                config = init_config.copy()
            else:
                config = best_config.copy()

            ys = {}
            for val in configs[param]:

                print(f"Current best_config = {best_config} ({param}, {val})")

                # Iterate over the current parameter
                config[param] = val

                # Only run tests if current val is different from best config's
                if config == best_config:
                    score = best_score
                    avg_max_fit = best_avg_max_fit
                    print("Already ran this test")
                else:
                    print("Running new test")
                    score, avg_max_fit = self.run_genetic_algorithm(config, all_requests)

                logf.write(f"\tResults for {val}: max_fit={score[0]}, gen={score[1]}\n")
                logf.flush()

                # Keep track of avg max fit per generation for each value
                ys[val] = avg_max_fit

                global_max_fit, global_gen = score
                if global_max_fit > best_score[0]:
                    best_score = score
                    best_avg_max_fit = avg_max_fit
                    best_config = config.copy()
                elif global_max_fit == best_score[0]:
                    if global_gen < score[1]:
                        best_score = score
                        best_avg_max_fit = avg_max_fit
                        best_config = config.copy()

            logf.write(f"\tSelected {best_config[param]}\n")
            self.plot(param, ys, best_config)

    def generate_requests(self, number):
        requests = []
        for i in range(number):
            elev = np.random.randint(0, self.nr_elevators)
            src_floor = np.random.randint(0, self.nr_floors)
            dst_floor = np.random.randint(0, self.nr_floors)
            while src_floor == dst_floor:
                dst_floor = np.random.randint(0, self.nr_floors)
            requests.append((elev, (src_floor, dst_floor)))
        return requests

    def generate_init_population(self):
        population = np.random.randint(
            low=0,
            high=self.nr_elevators,
            size=(self.population_size, len(self.pending_reqs))
        )
        return population

    def substitution(self, population, prev_fittest_indiv, type='replace'):
        if type == 'replace':
            return population
        elif type == 'elitist':
            population[-1] = prev_fittest_indiv
            return population
        else:
            raise NotImplementedError

    def select(self, population, fitness, type='roulette'):
        if type == 'roulette':
            total_fitness = np.sum(fitness)
            samples = np.random.choice(
                a=np.arange(0, self.population_size),
                size=self.population_size,
                replace=True,
                p=[indiv_fitness/total_fitness for indiv_fitness in fitness]
            )
            new_population = np.take(population, samples, axis=0)
        elif type =='tournaments':
            new_population = []
            for _ in range(self.population_size):
                nr_members = round(self.population_size*0.25)
                members = np.random.choice(
                    a=np.arange(0, self.population_size),
                    size=nr_members,
                    replace=False
                )
                mini_population = np.take(population, members, axis=0)
                mini_fitness = np.take(fitness, members, axis=0)
                new_population.append(mini_population[mini_fitness.argmax()])
            return np.stack(new_population)
        else:
            raise NotImplementedError

        return new_population

    def crossover(self, population, type='one-point'):
        # If we only have one gene, crossover doesn't make sense
        if len(self.pending_reqs) <= 1:
            return population

        # If we have up to two elements, two-point crossover doesn't make sense
        if len(self.pending_reqs) <= 2 and type == 'two-point':
            type = 'one-point'

        new_population = []
        for i in range(0, self.population_size, 2):
            parents = population[i:i+2]
            if np.random.uniform() < self.crossover_rate:
                if type == 'one-point':
                    sep_index = np.random.randint(1, len(self.pending_reqs))
                    cross = [parents[:, :sep_index], parents[::-1, sep_index:]]
                elif type =='two-point':
                    sep_indices = np.random.choice(
                        a=np.arange(1, len(self.pending_reqs)),
                        size=2,
                        replace=False
                    )
                    min_index = np.min(sep_indices)
                    max_index = np.max(sep_indices)
                    cross = [
                        parents[:, :min_index],
                        parents[::-1, min_index:max_index],
                        parents[:, max_index:]
                    ]
                children = np.concatenate(cross, axis=1)
                new_population.append(children)
            else:
                new_population.append(parents)
        return np.concatenate(new_population)

    def mutation(self, population, type='flip'):
        mutation_mask = np.random.uniform(size=population.shape)
        rows, cols = np.where(mutation_mask < self.mutation_rate)
        for i, j in zip(rows, cols):
            if type == 'flip':
                cur_elev = population[i,j]
                rem_elevs = np.concatenate(
                    [np.arange(0, cur_elev), np.arange(cur_elev+1, self.nr_elevators)]
                )
                population[i,j] = np.random.choice(rem_elevs)
            elif type == 'swap':
                offset = np.random.randint(1, len(self.pending_reqs))
                j_next = (j+offset)%len(self.pending_reqs)
                tmp = population[i, j]
                population[i, j] = population[i, j_next]
                population[i, j_next] = tmp
            else:
                raise NotImplementedError
        return population

    def compute_fitness(self, population):
        fitness = np.zeros(shape=self.population_size, dtype=np.float64)
        for ind_id, individual in enumerate(population):
            journey_times = []
            elev_pending_reqs = [[] for _ in range(self.nr_elevators)]
            elev_floor_stops = [[] for _ in range(self.nr_elevators)]

            # Populate lists of pending requests for each elevator for the
            # current chromossome
            for i, req in enumerate(self.pending_reqs):
                elev_pending_reqs[individual[i]].append(req[1])

            # For each elevator, compute the sequence of stops
            for id in range(self.nr_elevators):
                self.calculate_stop_seq(id, elev_pending_reqs, elev_floor_stops)

                # Compute journey times for ongoing requests
                for req in self.elevators[id].ongoing_reqs:
                    prev_floor = self.elevators[id].cur_floor
                    total_floors, total_stops = 0, 0

                    if not self.elevators[id].traveling:
                        total_stops += 1

                    for stop_id, stop in enumerate(elev_floor_stops[id]):
                        total_stops += 1
                        total_floors += abs(stop-prev_floor)
                        prev_floor = stop
                        if stop == req:
                            break
                    journey_times.append(total_floors*self.inter_floor_time + total_stops*self.passive_time)

                # Compute journey times for pending requests
                for req in elev_pending_reqs[id]:
                    prev_floor = self.elevators[id].cur_floor
                    target_floor = req[0]   # Start looking for src floor
                    total_floors, total_stops = 0, 0

                    if not self.elevators[id].traveling:
                        total_stops += 1

                    for stop in elev_floor_stops[id]:
                        total_stops += 1
                        total_floors += abs(stop-prev_floor)
                        prev_floor = stop
                        if stop == target_floor:
                            if target_floor == req[0]:
                                target_floor = req[1]  # Start looking for dst floor
                            else:
                                break
                    journey_times.append(total_floors*self.inter_floor_time + total_stops*self.passive_time)
            avg_journey_time = np.mean(journey_times)
            if avg_journey_time > 0:
                fitness[ind_id] = 1/avg_journey_time
            else:
                fitness[ind_id] = np.inf
        return fitness

    def calculate_stop_seq(self, id, elev_pending_reqs, elev_floor_stops, verbose=False):
        fst_travel = set()
        snd_travel = set()
        trd_travel = set()
        for req in self.elevators[id].ongoing_reqs:
            fst_travel.add(req)
        for req in elev_pending_reqs[id]:
            src_floor, dst_floor = req
            cur_floor = self.elevators[id].cur_floor

            if self.elevators[id].direction == 1:
                if dst_floor > src_floor:
                    if src_floor > cur_floor:
                        fst_travel.add(src_floor)
                        fst_travel.add(dst_floor)
                    else:
                        trd_travel.add(src_floor)
                        trd_travel.add(dst_floor)
                else:
                    snd_travel.add(src_floor)
                    snd_travel.add(dst_floor)
            else:
                if dst_floor < src_floor:
                    if src_floor < cur_floor:
                        fst_travel.add(src_floor)
                        fst_travel.add(dst_floor)
                    else:
                        trd_travel.add(src_floor)
                        trd_travel.add(dst_floor)
                else:
                    snd_travel.add(src_floor)
                    snd_travel.add(dst_floor)

        fst_travel = list(fst_travel)
        snd_travel = list(snd_travel)
        trd_travel = list(trd_travel)

        ordered_stops = []
        if self.elevators[id].direction == 1:
            ordered_stops = sorted(fst_travel)
            ordered_stops.extend(sorted(snd_travel, reverse=True))
            ordered_stops.extend(sorted(trd_travel))
        else:
            ordered_stops = sorted(fst_travel, reverse=True)
            ordered_stops.extend(sorted(snd_travel))
            ordered_stops.extend(sorted(trd_travel, reverse=True))

        if ordered_stops:
            elev_floor_stops[id] = [ordered_stops[0]]
            for i in range(1, len(ordered_stops)):
                if ordered_stops[i] != ordered_stops[i-1]:
                    elev_floor_stops[id].append(ordered_stops[i])

class StoppingCriteria:
    def __init__(self, criteria, manager):
        self.criteria = criteria
        self.manager = manager
        self.generations = 0
        self.max_fit = 0
        self.stale_gens = 0

    def __call__(self, population, max_fit=None):
        if self.criteria == 'generations':
            if self.generations > self.manager.generations:
                return False
            self.generations += 1
            return True
        elif self.criteria == 'stale':
            if max_fit > self.max_fit:
                self.max_fit = max_fit
                self.stale_gens = 0
            else:
                self.stale_gens += 1

            if self.stale_gens > 30:
                return False
            else:
                return True

        elif self.criteria == 'similarity':
            if population is not None:
                bins = {}
                for indiv in population:
                    indiv = tuple(i for i in indiv)
                    bins[indiv] = bins.get(indiv, 0) + 1
                max_cnt = 0
                total_cnt = 0
                for cnt in bins.values():
                    if cnt > max_cnt:
                        max_cnt = cnt
                    total_cnt += cnt
                if max_cnt/total_cnt > 0.6:
                    return False
                else:
                    return True
            else:
                return True
        else:
            raise NotImplementedError
