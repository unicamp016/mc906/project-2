\section{Modelagem do Problema}\label{sec:model}

  Como o objetivo do trabalho é modelar o problema de alocação ótima de elevadores inteligentes usando algoritmos genéticos, foram feitas algumas suposições para tornar o problema mais interessante. Primeiro, supomos que ao realizar uma requisição, o usuário sempre especifica o andar de destino. Isso é uma suposição válida, já que muitos dos elevadores modernos já suportam isso e sem o andar destino só seria possível otimizar o tempo de espera de cada cliente ao invés do tempo total da jornada. Além disso, supomos que o sistema é de alocação contínua, isto é, o usuário não recebe uma resposta imediata a respeito de qual elevador está designado para ele e só saberá o seu elevador quando o mesmo chegar no seu andar. Apesar dos elevadores que requerem a especificação do andar destino empregarem tipicamente a alocação imediata, o uso da alocação contínua nos dá maior liberdade para otimizar o tempo total de jornada do usuário, já que um elevador não está comprometido ao usuário no momento em que este realiza a requisição. O principal problema com o nosso modelo é quando dois elevadores chegam no mesmo andar onde foi feita uma requisição e o usuário não saberá em qual deve entrar. Isso pode ser sanado se assumirmos a existência de um aplicativo que informa o usuário a respeito do seu elevador designado. Uma outra forma de traduzir o modelo seria através de um display de senhas, onde o usuário recebe uma senha no momento em que realiza a requisição e deve ficar atento ao display para saber qual elevador foi designado para ele. Além disso, na implementação, assume-se que cada requisição corresponde a um único passageiro, o que poderia ser traduzido para um modelo real através de uma câmera que conta o número de pessoas em cada andar e gera o número de requisições correspondentes ao número de passageiros para o sistema de controle.

  Além disso, para tornar o nosso modelo mais fiel ao funcionamento habitual dos elevadores, adotamos as seguintes regras para o nosso sistema~\cite{closs1970control}:
  \begin{enumerate}
    \item Um elevador não pode passar por um andar onde um passageiro deseja sair;
    \item Os andares de destino do elevador são servidos sequencialmente de acordo com a direção do movimento do elevador;
    \item Um elevador carregando passageiros não pode mudar de direção se houver pelo menos um passageiro dentro do mesmo.
  \end{enumerate}

  Essas regras impedem-nos de adotar uma modelagem onde o elevador pode escolher um percurso arbitrário para entregar os passageiros e permite a aplicação dos operadores de crossover e mutação sem com que haja uma preocupação em gerar indivíduos inválidos, já que dado um conjunto de requisições designadas para um elevador, só existe um percurso possível para o mesmo~\cite{sobral2011elevadores}.

  % * Capacidade infinita
  % * Usuário especifica a direção
  % * Não tem alocação imediata
  % * O elevador vai tudo para cima antes de ir para baixo
  % * Tem um painel que fala para que elevador você tem que ir
  % * Tem uma câmera que conta quantas pessoas correspondem a cada requisicao
  % * Demora um tempo constante para o pessoal entrar no elevador

  \subsection{Estrutura dos Cromossomos}

  A partir do momento que um passageiro entra em um elevador, não é mais possível modificar o elevador designado para ele. O algoritmo genético só pode alterar o elevador designado para as requisições pendentes, mas deve levar em consideração no cálculo da função de aptidão os passageiros que já estão dentro de cada elevador. Para otimizar a alocação de elevadores para as requisições pendentes, definimos a estrutura do \textbf{cromossomo} como um vetor de inteiros, onde os \textbf{alelos} são os números inteiros entre 0 e $m-1$, em que $m$ é o número de elevadores no sistema, e o \textbf{locus} nos diz a requisição para qual o elevador foi designado. O tamanho do cromossomo depende do número de requisições. Dado um conjunto de $n$ requisições pendentes e uma \textbf{população} de $p$ indivíduos, podemos representar a população através de uma matriz de dimensões $p \times n$ de inteiros sem sinal. A Figura~\ref{fig:population-example} mostra um exemplo de uma população para o nosso modelo.

  \begin{figure}[!htbp]
    \centering
    \begin{tikzpicture}
      \matrix[matrix of nodes, style={nodes={rectangle,draw}}, minimum height=1.5em, row sep=-\pgflinewidth, column sep=-\pgflinewidth]{
        1 & 2 & 3 & 0 & 2 & 7 & 6 & 3 & 2 & 7\\
        4 & 5 & 6 & 1 & 0 & 5 & 2 & 2 & 0 & 1\\
        7 & 3 & 4 & 5 & 0 & 0 & 7 & 4 & 3 & 6\\%
      };
    \end{tikzpicture}
    \caption{Exemplo de uma população com $p=3$ indivíduos, $n=10$ requisições pendentes e um sistema com $m=8$ elevadores.}\label{fig:population-example}
  \end{figure}

  Para facilitar a implementação do modelo, também supusemos que os elevadores têm capacidade infinita, já que restringir a capacidade dos elevadores requer uma verificação custosa a respeito da validade dos indivíduos no algoritmo genético. Para cada invíduo gerado, seria necessário montar a sequência de paradas de cada elevador no sistema e verificar se em algum momento nessa sequência o número de indíviduos dentro do elevador ultrapassa o limite imposto. Supondo que a capacidade é infinita permite-nos aplicar os operadores de crossover e mutação e sempre gerar indivíduos válidos.

  \subsection{Função de Aptidão}
  Ao desenvolver uma função de aptidão, é necessário realizar um estudo cuidadoso do que o sistema de controle de elevadores deve priorizar. Por exemplo, poderíamos priorizar o consumo de energia dos elevadores, proporcional ao tempo que ele passa transitando entre os andares. Também seria possível priorizar o tempo total de jornada dos passageiros, dando pesos diferentes ao tempo de espera pela chegada do elevador e ao tempo gasto dentro do mesmo. Por simplicidade, optou-se por minimizar o tempo total de jornada de cada requisição, dando pesos iguais ao tempo de espera por um elevador e o tempo dentro dele. A aptidão de um indivíduo é dada pela Equação~\ref{eq:fitness}, onde $T_{med}$ é o tempo médio de todas as requisições para todos os elevadores considerando as atribuições do indivíduo.

  \begin{equation}\label{eq:fitness}
    f = 1/T_{med}
  \end{equation}

  Podemos definir o valor de $T_{med}$ pela Equação~\ref{eq:average}, onde $m$ é o número de elevadores no sistema e $n_i$ é o total de requisições para o elevador $i$, incluindo as requisições pendentes e as em andamento.

  \begin{equation}\label{eq:average}
    T_{med} = \frac{\sum_{i=1}^m\sum_{j=1}^{n_i} T_{ij}}{\sum_{i=1}^m\sum_{j=1}^{n_i} 1}
  \end{equation}

  Para calcular o tempo de jornada de uma requisição $T_{ij}$, devemos primeiro estabelecer a ordem de paradas do elevador, para então contar quantos andares ele percorreu até chegar no andar destino do passageiro e quantas paradas foram necessárias. Dadas essas informações o tempo é de jornada é dado pela Equação~\ref{eq:journey} onde $d$ é o número de andares percorridos até o andar destino, $s$ é o número de paradas realizadas até a entrega do passageiro, $V_e$ é o número de andares percorridos por segundo (assumindo velocidade constante) e $T_p$ é o tempo gasto em uma parada.

  \begin{equation}\label{eq:journey}
    T_{ij} = d V_e + s T_p
  \end{equation}

  As constantes $V_e$ e $T_p$ são entradas para o problemas, mas o número de andares percorridos e o número de paradas deve ser computado para cada requisição. Para cada elevador, nós ordenamos as paradas seguindo as regras de funcionamento habitual dos elevadores discutidas no início dessa seção. Dada a direção atual de um elevador, primeiro cada requisição é classificada em três categorias diferentes, dependendo da passagem em que estima-se satisfazê-la. As passagens depois são ordenadas de acordo com a direção de viagem correspondnte e em seguida concatenadas para formar a sequência de paradas. A partir da sequência, basta contar quantos andares são percorridos até o andar destino e quantos paradas são feitas para então aplicar a Equação~\ref{eq:journey}.

  Vamos concretizar o algoritmo através de um exemplo. Suponha um prédio com 10 andares e estamos computando a aptidão para todas as requisições designadas para um elevador subindo e que encontra-se no andar 4. Para este elevador, $V_e = 2$ e $T_p = 4$. O elevador já tem duas requisições em andamento, com passageiros indo para os andares $r_{a_1} = 6$ e $r_{a_2} = 8$. Além disso, o elevador tem 4 requisições pendentes, caracterizadas pelas tuplas $(f_s, f_d)$, onde $f_s$ é o andar fonte e $f_d$ o andar destino, $r_{d_1} = (5, 6)$, $r_{d_2} = (7, 4)$, $r_{d_3} = (5, 2)$ e $r_{d_4} = (1, 9)$.

  O primeiro passo do algoritmo envolve determinar em qual passagem a requisição será satisfeita. Todas as requisições em andamento devem ser satisfeitas na primeira passagem, já que pelas regras definidas no início da seção, um elevador só pode mudar de direção quando não houver mais passageiros dentro dele. Se o elevador está subindo, todas as requisições pendentes cujo andar de partida é maior que o andar atual do elevador também serão satisfeitas na primeira passagem. Qualquer requisição de descida será satisfeita na segunda passagem e qualquer uma de subida com andar fonte menor que o andar atual será satisfeita somente na última passagem. Dessa forma, temos a seguinte classificação para o nosso exemplo:

  \begin{itemize}
    \item \textbf{1\textsuperscript{a} passagem:}  $r_{a_1} = 6$, $r_{a_2} = 8$, $r_{d_1} = (5, 6)$;
    \item \textbf{2\textsuperscript{a} passagem:}  $r_{d_2} = (7, 4)$, $r_{d_3} = (5, 2)$;
    \item \textbf{3\textsuperscript{a} passagem:}  $r_{d_4} = (1, 9)$.
  \end{itemize}

  Para simplificar a modelagem, o tempo gasto em uma parada é constante e independe do número de pessoas entrando e saindo do elevador naquele andar. Por isso, o próximo passo envolve a remoção de andares duplicados e a ordenação de acordo com a direção do elevador para a passagem. Neste caso, como a segunda passagem é de descida, ordenamos os andares em ordem decrescente.

  \begin{itemize}
    \item \textbf{1\textsuperscript{a} passagem:}  5, 6, 8;
    \item \textbf{2\textsuperscript{a} passagem:}  7, 5, 4, 2;
    \item \textbf{3\textsuperscript{a} passagem:}  1, 9.
  \end{itemize}

  Agora, basta concatenar as paradas para as passagens em ordem para obtermos a lista de paradas completa. Neste exemplo, temos que o elevador irá realizar as paradas 5, 6, 8, 7, 5, 4, 2, 1 e 9, nessa ordem. Tomemos como exemplo a requisição $r_{d_2}$ para entender o cálculo do tempo de jornada. O número de andares percorridos até satisfazer essa requisição é $d = |5-4| + |6-5| + |8-6| + |7-8| + |5-7| + |4-5| = 8$ e o número de paradas é $s = 6$, dado pelo índice do andar destino no vetor de paradas. Dessa forma, o tempo gasto para satisfazer a requisição $r_{d_2}$ é $T_{r_{d_2}} = 8\cdot2 + 6\cdot4 = 40$. A Tabela~\ref{tab:requests} mostra o número de andares percorridos, paradas feitas e o tempo total gasto para satisfazer todas as requisições do exemplo. O mesmo procedimento deve ser repetido para cada um dos elevadores, até obter o tempo de jornada para todas as requisições no sistema, a partir de onde pode-se aplicar as Equações~\ref{eq:average} e~\ref{eq:fitness} para obter a aptidão do indivíduo.

  \begin{table}[h!]
    \centering
    \begin{tabular}{|c|c|c|c|}
     \hline
     Requisição & Andares ($d$) & Paradas ($s$) & Tempo ($T$) \\\hline
     $r_{a_1}$ & 2 & 2 & 12 \\\hline
     $r_{a_2}$ & 4 & 3 & 20 \\\hline
     $r_{d_1}$ & 2 & 2 & 12 \\\hline
     $r_{d_2}$ & 8 & 6 & 40 \\\hline
     $r_{d_3}$ & 10 & 7 & 48 \\\hline
     $r_{d_4}$ & 19 & 9 & 74 \\\hline
    \end{tabular}
    \caption{Número de andares percorridos, paradas feitas e tempo total gasto para satisfazer as requisições do elevador do exemplo}\label{tab:requests}
  \end{table}
