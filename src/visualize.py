import os

import simpy
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.markers as markers

class AnimatedElevator:
    def __init__(self, iterations, env, elevator_manager, output_dir):
        self.env = env
        self.elevator_manager = elevator_manager
        self.nr_elevators = elevator_manager.nr_elevators
        self.nr_floors = elevator_manager.nr_floors
        self.iterations = iterations
        self.output_dir = output_dir

        # Get matplotlib objects
        self.fig, self.ax = plt.subplots()
        self.timer = self.ax.text(self.nr_elevators+1, self.nr_floors+0.5, '$t=0$')
        self.scatters = [self.ax.scatter([], []) for _ in range(self.nr_elevators)]
        up_marker_obj = markers.MarkerStyle('^')
        self.up_path = up_marker_obj.get_path().transformed(up_marker_obj.get_transform())
        dw_marker_obj = markers.MarkerStyle('v')
        self.dw_path = dw_marker_obj.get_path().transformed(dw_marker_obj.get_transform())
        self.anim = animation.FuncAnimation(self.fig, self.run, self.data_gen, blit=False,
                                      interval=300, repeat=False, init_func=self.init_plot)

    def init_plot(self):
        self.ax.set_ylim(0, self.nr_floors)
        self.ax.set_xlim(0, self.nr_elevators+1)
        self.ax.set_ylabel('Floors')
        self.ax.set_xlabel('Elevators')
        xtlabels = [str(i) for i in range(self.nr_elevators+2)]
        xtlabels[0] = ''
        xtlabels[-1] = ''
        self.ax.set_xticklabels(xtlabels)

    def data_gen(self):
        iter = 1
        while iter <= self.iterations:
            self.env.run(until=iter)
            self.timer.set_text(f'$t={iter}$')
            elevators = self.elevator_manager.elevators
            elev_data = [{} for _ in range(self.nr_elevators)]
            for i, elev in enumerate(elevators):
                elev_data[i]['id'] = elev.id + 1
                elev_data[i]['cur_floor'] = elev.cur_floor
                elev_data[i]['direction'] = elev.direction
                elev_data[i]['passengers'] = len(elev.ongoing_reqs)
                elev_data[i]['traveling'] = elev.traveling
            yield elev_data
            iter += 1

    def run(self, elev_data):
        for i, data in enumerate(elev_data):
            self.scatters[i].set_offsets(np.c_[data['id'], data['cur_floor']])
            if data['traveling']:
                self.scatters[i].set_color('g')
            else:
                self.scatters[i].set_color('r')
            if data['direction']:
                self.scatters[i].set_paths((self.up_path,))
            else:
                self.scatters[i].set_paths((self.dw_path,))
            self.scatters[i].set_sizes([40 + 30*data['passengers']])

    def show(self):
        plt.waitforbuttonpress()

    def save(self):
        video_fn = os.path.join(self.output_dir, 'elevator_sim.avi')
        self.anim.save(video_fn, writer='ffmpeg')
