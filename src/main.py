import argparse
import simpy
import manager
import visualize

def main(output_dir, log_dir, floors, elevators, show):
    run_time = 200
    env = simpy.Environment()
    elev_manager = manager.ElevatorManager(
        env=env,
        nr_floors=floors,
        nr_elevators=elevators,
        arrival_rate=2,
        passive_time=4,
        generations=300,
        population_size=30,
        inter_floor_time=2,
        crossover_rate=0.7,
        mutation_rate=0.01,
        output_dir=output_dir,
        log_dir=log_dir
    )

    if show:
        animated_elev = visualize.AnimatedElevator(run_time, env, elev_manager, output_dir)
        animated_elev.save()
    else:
        configs = {
            'population-size': [30, 50],
            'crossover-rate': [0.7, 0.9],
            'mutation-rate': [0.01, 0.05],
            'stopping-criteria': ['generations','stale'],
            'crossover': ['one-point', 'two-point'],
            'mutation': ['flip', 'swap'],
            'substitution': ['elitist', 'replace'],
            'selection': ['roulette', 'tournaments']
        }
        elev_manager.compare_genetic_algorithms(configs, iterations=50, nr_requests=100)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output_dir", type=str, default='bin')
    parser.add_argument("-l", "--log_dir", type=str, default='logs')
    parser.add_argument("-f", "--floors", type=int, default=20)
    parser.add_argument("-e", "--elevators", type=int, default=4)
    parser.add_argument("--show", action='store_true')
    args = parser.parse_args()

    kwargs = vars(args)
    main(**kwargs)
